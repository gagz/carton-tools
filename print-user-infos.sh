#!/bin/sh

user="$1"
total_used_space="$(du -hs /srv/storage/$user/ | awk '{print $1}')"

text="""Taille totale occupée: $total_used_space
"""

# Si la variable $USER est vide ou n'existe pas, on affiche le html
# (car le LUA de nginx n'a qu'une seul envvar, PWD)
if [ -z "$USER" ]; then
	cat <<-EOF
	<!doctype html>
	<html>
	<head>
		<meta charset="utf-8" />
	</head>
	<body>
		$text
	</body>
	</html>
	EOF
else
	echo $text
fi
