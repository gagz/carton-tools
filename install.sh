#!/bin/sh

SCRIPTPATH="$(realpath "$(dirname $0)")"

echo INSTALL PACKAGES
sh "${SCRIPTPATH}/inc/__install-packages.sh"

echo INSTALL GOSSA
sh "${SCRIPTPATH}/inc/__install-gossa.sh"

echo CREATE TRANSMISSION-DAEMON SYSTEMD UNIT
cat > /etc/systemd/system/transmission@.service <<-EOF
	[Unit] 
	Description=Transmission BitTorrent Daemon for %i
	After=network.target
	
	[Service]
	User=%i
	Group=%i
	Type=simple
	ExecStart=/usr/bin/transmission-daemon -f --log-error --logfile /home/%i/.config/transmission-daemon/transmission.log
	ExecReload=/bin/kill -s HUP 
	
	[Install]
	WantedBy=multi-user.target
EOF

echo CONFIGURE NGINX
rm /etc/nginx/sites-enabled/default
cp "${SCRIPTPATH}/assets/nginx-carton.conf" /etc/nginx/sites-available/carton
ln -s /etc/nginx/sites-available/carton /etc/nginx/sites-enabled/carton
cp "${SCRIPTPATH}/assets/index.html" "${SCRIPTPATH}/assets/changepass.html" /var/www/html/

echo "Make sure you have mounted the storage on /mnt/storage and update /etc/fstab accordingly"
echo "You should restart nginx"

exit


# Done for the carton migration in 2023
for i in $(cut -f1 -d: /mnt/old-seedbox/etc/seedbox-users); do \
	/opt/carton-tools/add-user.sh $i bleh; \
	systemctl stop transmission@$i.service; \
	'cp' -va --force /mnt/old-carton/home/$i/.config/transmission-daemon /home/$i/.config/; \
	chown -R "$i":"$i" /home/$i/.config/transmission-daemon; \
	chown -R "$i":"$i" "/srv/storage/$i"
	systemctl start transmission@$i.service; \
done
cp /mnt/old-carton/etc/seedbox-users /etc/

