#!/bin/bash

DESTDIR=/opt/

# Download Gossa from github (I want a packaaaage)
dlurl="$(curl -sNL \
	https://api.github.com/repos/pldubouilh/gossa/releases/latest \
       	| jq -r '.assets[]?.browser_download_url' | grep linux-x64)"

wget "$dlurl" -O /opt/gossa
chmod +x /opt/gossa

# systemd users config (quotes around EOF to avoid var expansion)
cat > /etc/systemd/system/gossa@.service <<-'EOF'
	[Unit]
	Description=Gossa
	After=network.target
	
	[Service]
	User=%i
	Group=%i
	UMask=002
	
	Type=simple
	WorkingDirectory=/opt
	ExecStart=sh -c '/opt/gossa -h=127.0.0.1 -p="$(head -1 /etc/nginx/sites-enabled/gossa-%i | sed 's/^#//')" -prefix=/myfiles/ -k=false /srv/storage/%i'
	TimeoutStopSec=20
	KillMode=mixed
	Restart=always
	RestartSec=2
	
	[Install]
	WantedBy=multi-user.target
EOF

# systemd for /allfiles gossa
cat > /etc/systemd/system/gossa.service <<-'EOF'
	[Unit]
	Description=Gossa allfiles
	After=network.target
	
	[Service]
	User=www-data
	Group=www-data
	UMask=002
	
	Type=simple
	WorkingDirectory=/srv/storage/allfiles
	ExecStart=sh -c '/opt/gossa -h=127.0.0.1 -p=8000 -prefix=/allfiles/ -k=false -ro /srv/storage/allfiles'
	TimeoutStopSec=20
	KillMode=mixed
	Restart=always
	RestartSec=2
	
	[Install]
	WantedBy=multi-user.target
EOF

systemctl enable --now gossa.service
