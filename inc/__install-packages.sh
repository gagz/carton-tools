apt update && apt install \
	nginx \
	libnginx-mod-http-lua \
	transmission-daemon \
	apache2-utils \
	grep \
	curl \
	jq \
	wget \
	adduser \
	|| echo "ERROR: Couldn't update APT lists"
