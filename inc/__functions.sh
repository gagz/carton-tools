#!/bin/bash


### Functions ###

## Transmission ##

# Loop into users transmission configs to get the highest port
# in use among `transmission-daemon`s.
# Use the provided default if it's the first user
#
# Synopsis: _get_next_port_from_json 'rpc-port' '9091'
# or: _get_next_port_from_json 'peer-port' 51314
_transmission_get_next_port_from_json() {
	local key="$1"
	local defaultport="$2"
	local relpath=".config/transmission-daemon/settings.json"
	local highestvalue="$defaultport"
	local wasincreased='false'
	
	for d in /home/*/; do
		value=""
		confpath="${d}${relpath}"
		if [[ -f $confpath ]]; then
			paramrr=".[\"$key\"]"
			value=$(jq -r "$paramrr" < "$confpath")
		fi
		if [[ -n $value ]]; then
			if [[ $value -ge $highestvalue ]]; then
				highestvalue=$value
				wasincreased='true'
			fi
		fi
	done
	
	if [[ $wasincreased = 'true' ]]; then
	    ((highestvalue++))
	fi
	
	echo "$highestvalue"
}


# Create transmission conf directories
# Synopsis: _transmission_mkdir <username>
_transmission_mkdir() {
	local user="$1"
	
	if ! sudo -u "$user" mkdir -p /srv/storage/${user}; then
		return 1
	fi

	# the %%: strips the longest match of '/*' from $download_dir
	#chown ${user}:${user} -R /home/${user}/${download_dir%%/*}
	if ! sudo -u "$user" mkdir -p \
	    /home/${user}/.config/transmission-daemon/{blocklists,resume,torrents}; then
		return 1
	fi
	#chown ${user}:${user} -R /home/${user}/.config
	
	#if [[ $incomplete_dir_enabled = "true" ]]; then
	#    mkdir -p /home/${user}/${incomplete_dir}
	#    chown ${user}:${user} -R /home/${user}/${incomplete_dir%%/*}
	#fi
	
	#if [[ $watch_dir_enabled = "true" ]]; then
	#    mkdir -p /home/${user}/${watch_dir}
	#    chown ${user}:${user} -R /home/${user}/${watch_dir%%/*}
	#fi
}

# Create transmission-daemon file conf
# Synopsis: _transmission_mkconf username peer_port rpc_port rpc_password
_transmission_mkconf() {
	local user="$1"
	local peer_port="$2"
	local rpc_port="$3"
	local rpc_password="$4"
	
	# So that users cannot change settings permanently
	touch "/home/${user}/.config/transmission-daemon/settings.json"
	chmod a-w "/home/${user}/.config/transmission-daemon/settings.json"

	cat > "/home/${user}/.config/transmission-daemon/settings.json" <<-EOF
	{
		"alt-speed-down": 50,
		"alt-speed-enabled": false,
		"alt-speed-time-begin": 540,
		"alt-speed-time-day": 127,
		"alt-speed-time-enabled": false,
		"alt-speed-time-end": 1020,
		"alt-speed-up": 50,
		"bind-address-ipv4": "0.0.0.0",
		"bind-address-ipv6": "::",
		"blocklist-enabled": false,
		"blocklist-url": "http://www.example.com/blocklist",
		"cache-size-mb": 4,
		"dht-enabled": true,
		"download-dir": "/srv/storage/${user}",
		"download-limit": 100,
		"download-limit-enabled": 0,
		"download-queue-enabled": true,
		"download-queue-size": 5,
		"encryption": 1,
		"idle-seeding-limit": 30,
		"idle-seeding-limit-enabled": false,
		"incomplete-dir": "/srv/storage/${user}",
		"incomplete-dir-enabled": false,
		"lpd-enabled": false,
		"max-peers-global": 200,
		"message-level": 1,
		"peer-congestion-algorithm": "",
		"peer-id-ttl-hours": 6,
		"peer-limit-global": 200,
		"peer-limit-per-torrent": 50,
		"peer-port": ${peer_port},
		"peer-port-random-high": 65535,
		"peer-port-random-low": 49152,
		"peer-port-random-on-start": false,
		"peer-socket-tos": "default",
		"pex-enabled": true,
		"port-forwarding-enabled": false,
		"preallocation": 1,
		"prefetch-enabled": true,
		"queue-stalled-enabled": true,
		"queue-stalled-minutes": 30,
		"ratio-limit": 2,
		"ratio-limit-enabled": false,
		"rename-partial-files": true,
		"rpc-authentication-required": false,
		"rpc-bind-address": "127.0.0.1",
		"rpc-enabled": true,
		"rpc-host-whitelist": "*",
		"rpc-host-whitelist-enabled": true,
		"rpc-password": "${rpc_password}",
		"rpc-port": ${rpc_port},
		"rpc-url": "/transmission/",
		"rpc-username": "${user}",
		"rpc-whitelist": "",
		"rpc-whitelist-enabled": false,
		"scrape-paused-torrents-enabled": true,
		"script-torrent-done-enabled": false,
		"script-torrent-done-filename": "",
		"seed-queue-enabled": false,
		"seed-queue-size": 10,
		"speed-limit-down": 100,
		"speed-limit-down-enabled": false,
		"speed-limit-up": 100,
		"speed-limit-up-enabled": false,
		"start-added-torrents": true,
		"trash-original-torrent-files": false,
		"umask": 18,
		"upload-limit": 100,
		"upload-slots-per-torrent": 14,
		"utp-enabled": true,
		"watch-dir": "/home/${user}/",
		"watch-dir-enabled": false
	}
	EOF
	# If the `cat` fails, its status will be returned by the function
}


## Nginx ##

# Create the simple upstream config for transmission
# Synopsis: _nginx_mkconf_transmission USER TRANSMISSION_PORT
_nginx_mkconf_transmission() {
	local user="$1"
	local rpc_port="$2"
	cat > /etc/nginx/sites-available/transmission-$user <<-CONF
		upstream ${user}.transmission {
			server 127.0.0.1:${rpc_port};
		}
	CONF
	if [ $? -ne 0 ]; then
		return 1
	fi
	ln -s /etc/nginx/sites-available/transmission-$user \
		/etc/nginx/sites-enabled/transmission-$user
	# If the `ln` fails, its status will be returned by the function
}

# Create the simple upstream config for gossa
# Synopsis: _nginx_mkconf_gossa USER TRANSMISSION_PORT
_nginx_mkconf_gossa() {
	local user="$1"
	local port="$2"
	cat > /etc/nginx/sites-available/gossa-$user <<-CONF
		#$port
		upstream ${user}.gossa {
			server 127.0.0.1:${port};
		}
	CONF
	if [ $? -ne 0 ]; then
		return 1
	fi
	ln -s /etc/nginx/sites-available/gossa-$user \
		/etc/nginx/sites-enabled/gossa-$user
	# If the `ln` fails, its status will be returned by the function
}

# Add a line to /etc/seedbox-users
# Synopsis: _nginx_add_user_to_htpass_file USERNAME PASSWORD
_nginx_add_user_to_htpass_file() {
	local htfile='/etc/seedbox-users'
	local u="$1"
	local p="$2"
	local create=""

	# If file doesn't exist, we need to append -c to htpasswd
	if [ ! -f "$htfile" ]; then
		create="-c"
	fi
	htpasswd -b $create "$htfile" "$u" "$p" >/dev/null 2>&1
}




## Overlay ##

# Add the user in the overlay and remount it, or create the overlay
# and do not mount it, as with only one user it will fail to mount
#
# Synopsis: _add_user_to_overlay USERNAME
_add_user_to_overlay() {
	user="$1"

	# That's where user downloads dirs are, as well as the allfiles/ mountpoint
	# "noauto,x-systemd.automount" mount options are necessary to prevent
	# systemd from hanging on boot because it failed to mount the overlay.
	# The overlay is now mounted whenever it is first accessed and requests
	# are buffered until it is ready. See
	# https://wiki.archlinux.org/title/Fstab#Automount_with_systemd
	local storage_dir="/srv/storage"
	local fstab_line="overlay_allfiles $storage_dir/allfiles overlay noauto,x-systemd.automount,lowerdir="

	# XXX: this logic is shitty: if we only have one user, the fstab will be broken
	# If the line doesn't exist yet
	if ! grep -q "$fstab_line" /etc/fstab; then
		echo $fstab_line$storage_dir/$user | tee -a /etc/fstab >/dev/null || return
		# We do not want to mount the overlay just yet, as it will fail
		# because "at least 2 lowerdir needed"

	# If line is already there, let's edit it
	else
		# Overlayfs wont take '-o remount' seriously (lowerdir is not updated)
		# So we have to unmount it, edit fstab, and mount it again
		if grep -q 'overlay_allfile' /etc/mtab; then
			# if gossa is running we might get a "device in use" error
			systemctl stop gossa.service
			echo "Stopped gossa.service"
			umount overlay_allfiles || return
			systemctl start gossa.service
			echo "Started gossa.service"

		fi
		sed -i "s#^\(overlay_allfiles.*\)#\1:${storage_dir}/${user}#" /etc/fstab \
			|| return
		mount --source overlay_allfiles
	fi
}
