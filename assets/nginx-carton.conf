server {
        listen 80 default_server;
        #listen [::]:80 http2 default_server;
        server_name _;
        client_max_body_size 40G;
        server_tokens off;

        location ~ /\.ht {
                deny all;
        }

        location = /allfiles {
                return 301 https://$host/allfiles/;
        }

        # dedicated bloc (not in "location /") to bypass basic auth
        location /allfiles/ {
                root /var/www/html;

                #alias /srv/storage/allfiles/;
                #autoindex on;

                proxy_pass_request_headers on;
                proxy_set_header Host $host;
                proxy_http_version 1.1;

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Protocol $scheme;
                proxy_set_header X-Forwarded-Host $http_host;

                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection $http_connection;

                # Rewrite headers that contain http to have them https
                proxy_redirect http:// https://;

                proxy_buffering off;

                proxy_pass http://127.0.0.1:8000;
        }

        location / {
                root /var/www/html;
                auth_basic "What's the password?";
                auth_basic_user_file /etc/seedbox-users;

                location /myfiles {
                        proxy_pass_request_headers on;
                        proxy_set_header Host $host;
                        proxy_http_version 1.1;

                        proxy_set_header X-Real-IP $remote_addr;
                        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                        proxy_set_header X-Forwarded-Proto $scheme;
                        proxy_set_header X-Forwarded-Protocol $scheme;
                        proxy_set_header X-Forwarded-Host $http_host;

                        proxy_set_header Upgrade $http_upgrade;
                        proxy_set_header Connection $http_connection;

                        # Rewrite headers that contain http to have them https
                        #proxy_redirect http://$remote_user.gossa https://carton.0id.org;
                        proxy_redirect http:// https://;

                        proxy_buffering off;

                        proxy_pass http://$remote_user.gossa;
                }

                location = /transmission {
                        return 301 https://$host/transmission/web/;
                }

                location /transmission/ {
                        proxy_set_header X-Real-IP $remote_addr;
                        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                        proxy_set_header Host $http_host;
                        proxy_set_header X-NginX-Proxy true;
                        proxy_http_version 1.1;
                        proxy_set_header Connection "";
                        proxy_pass_header X-Transmission-Session-Id;
                        add_header   Front-End-Https   on;

                        proxy_set_header Authentication "";
                        proxy_pass http://$remote_user.transmission;

                        # Rewrite headers that contain http to have them https
                        proxy_redirect http:// https://;
                }
                location = /getinfos {
                        default_type text/html;
                        # needs libnginx-mod-http-lua
                        content_by_lua_block {
                                fd=io.popen('sh /opt/carton-tools/print-user-infos.sh ' .. ngx.var.remote_user)
                                ngx.say(fd:read('a'))
                        }
                }
                location = /changepass {
                        default_type text/html;
                        # needs libnginx-mod-http-lua
                        content_by_lua_block {
                                ngx.req.read_body()
                                local args, err = ngx.req.get_post_args()

                                if err == "truncated" then
                                        -- one can choose to ignore or reject the current request here
                                        ngx.say("Error in headers: ", err)
                                        return
                                end

                                if not args then
                                    ngx.say("failed to get post args: ", err)
                                    return
                                end

                                if ( args['new1'] == nil or args['new2'] == nil or args['current'] == nil ) then
                                        ngx.say("Missing arguments")
                                        return
                                end

                                if ( args['new1'] == args['new2'] ) then
                                        fd=io.popen('sh /opt/carton-tools/changepass.sh ' .. ngx.var.remote_user ..
                                                ' ' .. args['current'] .. ' ' .. args['new1'] )
                                        ngx.say(fd:read('a'))
                                else
                                        ngx.say("the two provided new passwords don't match")
                                end
                        }
                }
        }
}

