#!/bin/bash
# Many things taken from Flying_sausages' work for Swizzin 2020
# Extracted from swizzin in may 2022

#########
# Synopsis: ./this_script.sh "new-username" "new-password"
#########


### Check for missing commands ###
missing_commands=0
for c in jq htpasswd transmission-daemon nginx wget; do
	if ! command -v $c > /dev/null; then
		echo "Missing command $c"
		missing_commands=1
	fi
done

if [ $missing_commands -ne 0 ]; then
	echo "Exiting"
	exit 1
fi


### Args ###
if [ $# -ne 2 ]; then
	echo "Not enough arguments"
	echo
	echo "Usage: $0 USERNAME PASSWORD"
	exit 1
fi


### Our functions
source inc/__functions.sh


## Job starts here

newuser="$1"
newpassword="$2"


### Default ports
default_rpc_port="10000"
default_peer_port="10500"
filebrowser_port_increment_from_rpc="1000"


### get new ports
newrpcport="$(_transmission_get_next_port_from_json 'rpc-port' $default_rpc_port)"
newpeerport="$(_transmission_get_next_port_from_json 'peer-port' $default_peer_port)"


### Too many users already?
if [ "$newrpcport" -eq "$default_peer_port" ]; then
	echo "Too many users, cannot create new one. Exiting"
	exit 1
fi


### System user creation ###
# TODO: use `id` instead?
if grep -q "^$newuser:" /etc/passwd; then
	echo "$newuser already exists. Exiting"
	exit 1
fi

if ! adduser --force-badname --disabled-password --gecos "" "$newuser" >/dev/null; then
	echo "Failed to create user $newuser. Exiting"
	exit 1
fi


### Transmission config ###
if ! _transmission_mkdir $newuser; then
	echo "Failed to create transmission directories for user $newuser. Exiting"
	exit 1
fi

# We use the same password for all transmission-daemon instances, making password
# change simpler, while not loosing on security as transmission-daemon is behind
# nginx reverse-proxy, itself behind HTTP basic auth
if ! _transmission_mkconf "$newuser" "$newpeerport" "$newrpcport" "{95b70e4d2dc3e493d8395599377223c141394a93hYSJfjy5"; then
	echo "Failed to create transmission config file for user $newuser. Exiting"
	exit 1
fi

if ! systemctl -q enable transmission@${newuser}.service; then
	echo "Failed to enable transmission unit for $newuser. Exiting"
	exit 1
fi

if ! systemctl start transmission@$newuser.service; then
	echo "Failed to start transmission unit for $newuser. Exiting"
	exit 1
fi


### Nginx config ###

if ! _nginx_mkconf_transmission "$newuser" "$newrpcport"; then
	echo "Failed to create nginx transmission upstream proxy conf for $newuser. Exiting"
	exit 1
fi

new_gossa_port="$((newrpcport+filebrowser_port_increment_from_rpc))"
if ! _nginx_mkconf_gossa "$newuser" "$new_gossa_port"; then
	echo "Failed to create nginx gossa upstream proxy conf for $newuser. Exiting"
	exit 1
fi

if ! _nginx_add_user_to_htpass_file "$newuser" "$newpassword"; then
	echo "Failed to add $newuser to the passfile. Exiting"
	exit 1
fi

if ! systemctl -q reload nginx; then
	echo "Failed to reload nginx. Exiting"
	exit 1
fi


### Gossa file explorer ###

if ! systemctl -q enable gossa@$newuser.service; then
	echo "Failed to enable gossa unit for $newuser. Exiting"
	exit 1
fi

if ! systemctl start gossa@$newuser.service; then
	echo "Failed to start gossa unit for $newuser. Exiting"
	exit 1
fi


### Overlayfs for /allfiles ###
if ! _add_user_to_overlay "$newuser"; then
	echo "Failed to add \"$newuser\" to overlay. Exiting"
	exit 1
fi

### End ###
echo "Everything went fine, user $newuser successfully created"
