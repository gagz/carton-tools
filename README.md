# Carton-tools

*A set of tools to manage a simple multi-user seedbox*

## Features

* Multiuser
* Users have their own Transmission instance
* All files are publicly visible by everyone who knows the link
* Use systemd features to run instances
* Code is only simple bash and easy to read and understand

## Use

*Run commands as root*

* `./add-user.sh`:

## Install

*Installation has only been tested under Debian 11 and 12*
*Carton-tools depends on **systemd***

* Install sudo and git
  * `apt install sudo git`
* Clone the repo
  * `git clone https://0xacab.org/gagz/carton-tools`
* Install
  * `./install.sh`

