#!/bin/bash
# XXX: add question to delete orphaned files

user="$1"

read -n1 -p "Are you sure you want to delete user $user ? (y/n) " answer
echo
if [ "$answer" != "y" ]; then
	exit
fi


## Transmission

systemctl stop transmission@$user.service
systemctl disable transmission@$user.service


# Gossa filebrowser

echo "Removing user from filebrowser config"
systemctl disable --now gossa@$user.service


## System user

if ! deluser --remove-home "$user"; then
	echo "Failed to remove user $user."
fi


## Nginx config

echo "Removing nginx config for $user"
rm -f /etc/nginx/sites-available/{transmission,gossa}-$user
rm -f /etc/nginx/sites-enabled/{transmission,gossa}-$user

htpasswd -D /etc/seedbox-users "$user"

systemctl reload nginx


## Overlay

echo "Removing user from overlay"
umount overlay_allfiles
storage_dir="/srv/storage"

# -n: do not print pattern space (ie the whole working file)
# #p: print only matching line
lowerdirs="$(sed -n """s#^overlay_allfiles.*\=\(.*\)#\1#p""" /etc/fstab)"
newlowerdirs=""
nld_count=0

OLDIFS=$IFS
IFS=":"
for ld in $lowerdirs; do
	if [ "$ld" != "$storage_dir/$user" ]; then
		((nld_count++))
		if [ ! -n "$newlowerdirs" ]; then
			newlowerdirs="$ld"
		else
			newlowerdirs="${newlowerdirs}:${ld}"
		fi
	fi
done
IFS=$OLDIFS

echo $newlowerdirs

# If there are still users, apply new config
if [ -n "$newlowerdirs" ]; then
	sed -i "s#^\(overlay_allfiles.*=\).*#\1${newlowerdirs}#" /etc/fstab
	# Remount the overlay only if there are more than one 'lowerdir'
	if [ $nld_count -gt 1 ]; then
		mount --source overlay_allfiles
	fi
# otherwise remove the whole line
else
	# remove whole line
	echo "No more users, let's remove the whole line"
	sed -i "/^overlay_allfiles/d" /etc/fstab
fi


read -n1 -p "Delete /srv/storage/$user ? (y/n) " answer
echo
if [ "$answer" = "y" ]; then
	echo "Removing folder"
	rm -rf /srv/storage/$user/
else
	echo "Not removing folder"
fi
