#!/bin/sh

if [ $# -ne 3 ]; then
	echo "wrong number of arguments"
	exit 1
fi

htfile="/etc/seedbox-users"
username="$1"
oldpass="$2"
newpass="$3"

# verify provided password matches given username
if ! htpasswd -b -v "$htfile" "$username" "$oldpass" >/dev/null 2>&1; then
	text="Username or password incorrect"
else
	# update password in htfile
	if htpasswd -b "$htfile" "$username" "$newpass" >/dev/null 2>&1; then
		# First password change happened successfully
		text="HT Password updated successfully"
	else
		text="Error updating HT password, htpasswd exit code: $? ; args: $@ <br />Provided password was correct. <b>Please report to the admins!</b>"
	fi
fi


# Si la variable $USER est vide ou n'existe pas, on affiche le html
# (car le LUA de nginx n'a qu'une seul envvar, PWD)
if [ -z "$USER" ]; then
	cat <<-EOF
	<!doctype html>
	<html>
	<head>
		<meta charset="utf-8" />
		<title>Carton :: change password</title>
	</head>
	<body>
		<p>$text</p>
		<p><a href="/">Back to main page</a></p>
	</body>
	</html>
	EOF
else
	echo "$text"
fi
